import {
	requestNoAuth,
	request
} from '../../lib/java110/java110Request.js';
import url from '../../conf/url.js';

export function getCommunityName() {
	let _community = uni.getStorageSync("currentCommunityInfo");
	if(_community){
		return _community.communityName;
	}
	return "无";
}

export function getCommunityId() {
	let _community = uni.getStorageSync("currentCommunityInfo");
	if(_community){
		return _community.communityId;
	}
	return "-1"
}


export function queryFloors(_objData) {
	return new Promise((resolve, reject) => {
		requestNoAuth({
			url: url.queryFloors,
			data: _objData,
			method: "GET",
			//动态数据
			success: function(res) {
				let _json = res.data;
				if (_json.code == 0) {
					resolve(_json.data);
					return;
				}
				reject();
			},
			fail: function(e) {
				uni.hideLoading();
				uni.showToast({
					title: "服务器异常了",
					icon: 'none'
				})
			}
		});
	})
}
export function queryUnits(_objData) {
	return new Promise((resolve, reject) => {
		requestNoAuth({
			url: url.queryUnits,
			data: _objData,
			method: "GET",
			//动态数据
			success: function(res) {

				resolve(res.data);

			},
			fail: function(e) {
				uni.hideLoading();
				uni.showToast({
					title: "服务器异常了",
					icon: 'none'
				})
			}
		});
	})
}
export function queryRoomsByApp(_objData) {
	return new Promise((resolve, reject) => {
		requestNoAuth({
			url: url.queryRoomsByApp,
			data: _objData,
			method: "GET",
			//动态数据
			success: function(res) {
				let _json = res.data;
				if (_json.code == 0) {
					resolve(_json.data);
					return;
				}
				reject();
			},
			fail: function(e) {
				uni.hideLoading();
				uni.showToast({
					title: "服务器异常了",
					icon: 'none'
				})
			}
		});
	})
}
export function queryCommunitys(_objData) {
	return new Promise((resolve, reject) => {
		requestNoAuth({
			url: url.queryCommunitys,
			data: _objData,
			method: "GET",
			//动态数据
			success: function(res) {
				resolve(res.data);
			},
			fail: function(e) {
				uni.hideLoading();
				uni.showToast({
					title: "服务器异常了",
					icon: 'none'
				})
			}
		});
	})
}

export function getUserProperty(_objData){
	return new Promise((resolve, reject) => {
		request({
			url: url.listUserProperty,
			data: _objData,
			method: "GET",
			//动态数据
			success: function(res) {
				let _json = res.data;
				if (_json.code == 0) {
					resolve(_json.data);
					return;
				}
			},
			fail: function(e) {
			}
		});
	})
}

