package com.java110.hal.rtsp;

public interface IPlayVideo {

    /**
     * 设备ID传进来
     *
     * @param machineId
     * @return
     */
    String playVideo(String machineId);
}
